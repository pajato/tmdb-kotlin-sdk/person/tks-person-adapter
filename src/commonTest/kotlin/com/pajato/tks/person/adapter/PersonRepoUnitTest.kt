package com.pajato.tks.person.adapter

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbApiService.TMDB_BASE_API3_URL
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.InjectError
import com.pajato.tks.common.core.PersonKey
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.person.adapter.TmdbPersonRepo.getPerson
import com.pajato.tks.person.core.Person
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlin.test.fail

class PersonRepoUnitTest : ReportingTestProfiler() {
    private val apiKey = "8G9B0WX31T2U7P0Q"
    private val id = 653851
    private val key = PersonKey(id)
    private val url = "$TMDB_BASE_API3_URL/person/$id?api_key=$apiKey"
    private val resourceName = "person.json"
    private val urlConverterMap: Map<String, String> = mapOf(url to resourceName)

    private lateinit var errorMessage: String
    private lateinit var errorExc: String
    private lateinit var errorExcMessage: String
    private var isFetched: Boolean = false

    @BeforeTest fun setUp() {
        TmdbFetcher.inject(::fetch, ::handleError)
        TmdbFetcher.inject(apiKey)
        errorMessage = ""
        errorExc = ""
        errorExcMessage = ""
        isFetched = false
    }

    @Test fun `When accessing a non-cached person, verify person is fetched`() {
        val expectedPerson: Person = jsonFormat.decodeFromString(getJson(resourceName))
        runBlocking {
            assertEquals(expectedPerson, getPerson(key))
            assertTrue(isFetched)
        }
    }

    @Test fun `When simulating a Person fetch without injection, verify an inject error is thrown`() {
        val expected = "No api key has been injected!: No repo implementation has been injected!"
        TmdbFetcher.reset()
        runBlocking { assertFailsWith<InjectError> { getPerson(key) }.also { assertEquals(expected, it.message) } }
    }

    private fun fetch(key: String): String {
        val name = urlConverterMap[key] ?: fail("Key $key is not mapped to a test resource name!")
        isFetched = true
        return getJson(name).ifEmpty { throw IllegalStateException("No JSON available!") }
    }

    private fun getJson(name: String): String = javaClass.classLoader.getResource(name)?.readText() ?: ""

    private fun handleError(message: String, exc: Exception?) {
        errorMessage = message
        errorExc = if (exc == null) "null" else exc.javaClass.name
        errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
    }
}
