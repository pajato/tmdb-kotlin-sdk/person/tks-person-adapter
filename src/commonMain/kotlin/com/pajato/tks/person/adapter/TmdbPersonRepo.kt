package com.pajato.tks.person.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.PersonKey
import com.pajato.tks.person.core.Person
import com.pajato.tks.person.core.PersonRepo

/**
 * Repository object for fetching Person data from the TMDb (The Movie Database) API.
 *
 * Implements the [PersonRepo] interface to provide methods for asynchronous data retrieval.
 */
public object TmdbPersonRepo : PersonRepo {

    /**
     * Fetches a Person object from a remote source using the provided PersonKey.
     *
     * @param key The key representing the unique identifier for the Person to be fetched.
     * @return The fetched Person object.
     */
    override suspend fun getPerson(key: PersonKey): Person {
        val path = "person/${key.id}"
        val queryParams: List<String> = listOf()
        val serializer: Strategy<Person> = Person.serializer()
        return TmdbFetcher.fetch(path, queryParams, key, serializer, Person())
    }
}
