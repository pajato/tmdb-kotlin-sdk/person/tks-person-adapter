# tks-person-adapter

## Description

The Movie Database (TMDb) person feature, interface adapters layer, Kotlin MultiPlatform (KMP) common project,
providing Android, iOS and Desktop targets.

This project implements TMDb person interfaces in the interface adapters Clean Architecture layer. It exists to specify
these artifacts which are used by other TMDb interfaces and by outer layers to provide data acquired from the TMDb
database.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on the TMDb API, see
[The Movie Database API](https://developers.thepersondb.org/3/getting-started/introduction).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix   | Test Case Name                                                    |
|-------------------|-------------------------------------------------------------------|
| PersonRepo        | When accessing a person, verify behavior                          |
|                   | When accessing a non-cached person, verify behavior               |
|                   | When simulating a person fetch without injection, verify behavior |

### Overview

### Notes

The single responsibility for this project is to provide the TMDb person artifact definitions used by this
and outer architectural layers. The adapter layer implements these core person interfaces.


The TMDb Kotlin SDK (tks) entities (adapter) architectural layer for the person feature.

This project implements TMDb person interfaces from the innermost Clean Architecture layer. It exists to
realize the core TMDb person feature.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Started, no code committed yet.

## Documentation

For general documentation on the TMDB API, see
[The Person Database API](https://developers.thepersondb.org/3/getting-started/introduction).
